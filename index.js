// Determine of a single ACL entry is "dominated" by the users effective ACL
const acl_concrete_dominated = (concrete, effective) => {
  try {
    const [concreteOperations, concreteEndpoint] = concrete.split("/");
    const effectiveData = effective.split(":").reduce((acc, entity) => {
      const [values, key] = entity.split("/");
      acc[key] = values;
      return acc;
    }, {});

    const effectiveOperations = effectiveData[concreteEndpoint];
    return concreteOperations.split("").every(operation => {
      return effectiveOperations.includes(operation);
    });
  } catch (error) {
    throw `Error: ${error.message}`;
  }
};

// Truthy cases
console.log(acl_concrete_dominated("G/Time", "G/Time:GDP/Users"));
console.log(acl_concrete_dominated("D/Users", "G/Time:GDP/Users"));
console.log(acl_concrete_dominated("PD/Users", "G/Time:GDP/Users"));
// Falsy cases
console.log(acl_concrete_dominated("P/Time", "G/Time:GDP/Users"));
console.log(acl_concrete_dominated("H/Users", "G/Time:GDP/Users"));